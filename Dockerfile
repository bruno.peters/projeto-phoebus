FROM ubuntu:latest
RUN apt-get update && \
    apt-get upgrade && \
    apt-get install ansible wget unzip python3-pip -y && \
    pip3 install boto && \
    wget https://releases.hashicorp.com/terraform/1.0.4/terraform_1.0.4_linux_amd64.zip -O /tmp/terraform_1.0.4_linux_amd64.zip && \
    unzip /tmp/terraform_1.0.4_linux_amd64.zip -d /usr/bin

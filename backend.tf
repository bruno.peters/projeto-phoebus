terraform {
    backend "s3" {
        region = "us-east-2"
        bucket = "petclinic-backend"
        encrypt = "true"
        key = "terraform.tfstate"
    }
}
# Projeto Phoebus 

Projeto criado usando as principais ferramentas de automação do mercado para a automação e provisionamento de infraestrutura  até o deploy da aplicação 

## O que eu usei ?

Maven

Github

Docker

Ansible

Terraform

Drone CI

AWS

## Como isso funciona ?

Maven compila a aplicação em java 

Docker builda a aplicação em container

Terraform provisiona a infraestrutura na AWS

Ansible gerencia a configuração da instancia e roda a aplicação no container

Drone CI faz com que todas essas ferramentas trabalhem juntas para disponibizar a aplicação pronta  

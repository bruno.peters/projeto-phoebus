provider "aws" {
  region = "us-east-2"
}


resource "aws_key_pair" "petclinic-key" {
  key_name   = "petclinic-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCp2krxdMJYf0ebpqr+RFDL+Z5IRrsN2L+qET1bUQUD+eSLvXmzPmD9wUx9pSU1ghoQECNd9J+pGGteov3lFNay98AaVKPVfw5Zax9ReSToYKIjN97uCqv3TzltakNXuJsiXqmaNbZfnQshGi9gKfj5SL/5VoWE+3i4fk2tufM756+ZiBo+uXZk68n6mmBxfUzrUKB5SAGgGUP8KxSe59GXJIZnbQOGihKxFk7rYHVbWiXtbkb7LP4Fup/JeUl0tzf0fZ6BkDt6ZvA3C8aX4+4LuAbckUx76siGmd2J5fmc0GjAAueUKrtA0BPpqlJR3TqagxXZmYCGHEQxd4ZEsInROK8zud/BM88WsT6602Vw3Jhp8frIYzb4J3Hm5H1nv1ycQrSJlQzTgGIYWWOIU4y4Rnyz2MNpjuUhsNkWG4NyASbj8r+HDlzmjH3mt94hz1jyYZ0yedfqIr/HXGKp477V1uZV/2t3A60yg/xFOZ+LPc6WntuBVTpBDbyCgbFlCl0= bruno@bruno-Inspiron-5547"
}


resource "aws_security_group" "petclinic-sg" {
  
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

} 

resource "aws_instance" "petclinic" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  key_name = "petclinic-key"
  count = 1
  tags = {
    name = "petclinic"
  }
  security_groups = ["${aws_security_group.petclinic-sg.name}"]
}



